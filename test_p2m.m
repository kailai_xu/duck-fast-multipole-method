clear all ; close all
global X Y m
N = 50;
M = 40;
X = rand(N,3)-1;
Y = rand(M,3);
m = ones(M,1);

% X = [0,0,0];
% Y = [0.5,0.5,0.49];

% scatter3(X(:,1), X(:,2), X(:,3),'*');
% hold
% scatter3(Y(:,1), Y(:,2), Y(:,3),'o');

force = zeros(N,1);
for i = 1:N
    for j = 1:M
        force(i) = force(i) + m(j) / norm(Y(j,:) - X(i,:));
    end
end

xc = 0.5;  yc= 0.5; zc = 0.5;

multipole = p2m( (1:M)', xc, yc, zc );
val = evalPotential( (1:N)', multipole, xc, yc, zc);

norm(force-val)/norm(force)
plot(force,'*')
hold
plot(val,'o')

