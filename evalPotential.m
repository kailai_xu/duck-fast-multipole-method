function val = evalPotential(targets, multipole, xc, yc, zc)
% targets: the list of target objects in Particle
% multipole: the multipole array of the cell
% center: the point object of expansion center

global X 
x = X(targets,1);
y = X(targets,2);
z = X(targets,3);

R = sqrt(sum(([x,y,z] - [xc yc zc]).^2,2));
weights = [1./R, -(x-xc)./R.^3, -(y-yc)./R.^3, -(z-zc)./R.^3, ...
    3*(x-xc).^2./R.^5-1./R.^3, 3*(y-yc).^2./R.^5-1./R.^3, 3*(z-zc).^2./R.^5-1./R.^3, ...
    3*(x-xc).*(y-yc)./R.^5, 3*(y-yc).*(z-zc)./R.^5, 3*(x-xc).*(z-zc)./R.^5];
val = weights * multipole;

    