function get_multipole( root )
% Calculate multipole arrays for all leaf cells under cell p.
% traverse down recursively until leaf nodes

if root.nleaf
    root.multipole = p2m( root.leaf, root.xc, root.yc, root.zc);
else
    for i = 1:root.nchild
        get_multipole( root.child(i) );
    end
end