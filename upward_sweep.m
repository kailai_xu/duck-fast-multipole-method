function upward_sweep( cellList )
% UPWARD_SWEEP computes the multipoles of parent nodes from the leaf nodes
% and inner nodes using cellList
% Usage: 
% upward_sweep( cellList )
for i = length(cellList)-1:-1:1
    if cellList{i}.nchild
        m2m( cellList{i} );
    end
end