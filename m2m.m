function m2m( p )
% see notebook Ritz 108

children = p.child;
p.multipole = zeros(10,1);
for i = 1:length(children)
    c = children(i);
    dx = [p.xc - c.xc; 
        p.yc - c.yc;
        p.zc - c.zc];
    p.multipole = p.multipole + c.multipole;
    p.multipole(2:4) = p.multipole(2:4) + dx * c.multipole(1);
    p.multipole(5:7) = p.multipole(5:7) + ...
        dx .* c.multipole(2:4) + 0.5 * dx.^2 * c.multipole(1);
    p.multipole(8:10) = p.multipole(8:10) + ...
        0.5 * dx .* c.multipole([3;4;2]) + 0.5 * dx([2;3;1]) .* c.multipole(2:4) + ...
        0.5 * dx .* dx([2;3;1]) .* c.multipole(1);
end


