clear all
close all
global Y X m
M = 40;
X = rand(M,3)-1;
N = 50;
Y = rand(N,3);
m = ones(N,1);
t = fmmCell([0 0 0],[1 1 1],1:N,20,2);
CellList = t.constructCellList;

for i = 1:t.nchild
    t.child(i).multipole = p2m( t.child(i).leaf, t.child(i).xc, ...
        t.child(i).yc, t.child(i).zc);
end
m2m( t );

force0 = evalPotential( (1:M)', t.multipole, t.xc, t.yc, t.zc);

force = zeros(M,1);
for i = 1:M
    for j = 1:N
        force(i) = force(i) + m(j) / norm(Y(j,:) - X(i,:));
    end
end
plot(force,'*');
hold on
plot(force0,'o');
legend('direct sum', 'multipole')