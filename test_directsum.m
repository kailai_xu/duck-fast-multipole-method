clear all ; close all
global X Y m
N = 40;
X = rand(N,3);
Y = rand(N,3);
m = ones(N,1);

% scatter3(X(:,1), X(:,2), X(:,3),'*');
% hold
% scatter3(Y(:,1), Y(:,2), Y(:,3),'o');

force = zeros(N,1);
for i = 1:N
    for j = 1:N
        force(i) = force(i) + m(j) / norm(Y(j,:) - X(i,:));
    end
end




