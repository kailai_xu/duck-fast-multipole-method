clear all
close all
global Y X m
M = 1000; % number of particles
N = 1000; % number of sources
X = rand(M,3)-1;
Y = rand(N,3);
m = ones(N,1);
t = fmmCell([0 0 0],[1 1 1],1:N,20,inf);
cellList = t.constructCellList;

tic
get_multipole( t );
upward_sweep( cellList );
toc

tic
force0 = FMMEvalPotential( (1:M)', t, 0.5 );
% force0 = evalPotential( (1:M)', t.multipole, t.xc, t.yc, t.zc);
toc

tic
force = zeros(M,1);
for i = 1:M
    for j = 1:N
        force(i) = force(i) + m(j) / norm(Y(j,:) - X(i,:));
    end
end
toc

plot(force,'*');
hold on
plot(force0,'o');
legend('direct sum', 'multipole')