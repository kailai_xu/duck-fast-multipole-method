function val = FMMEvalPotential(targets, root, theta)
% Evaluate the gravitational potential at a target point i,
% caused by source particles cell p. If nleaf of cell p is less
% than n_crit (leaf), use direct summation. Otherwise (non-leaf), loop
% in p's child cells. If child cell c is in far-field of target particle i,
% use multipole expansion. Otherwise (near-field), call the function
% recursively.
global X Y m
val = arrayfun(@(i)helper(root,i,0), targets);

    function force = helper(cur, i, force)
        if cur.nleaf
            for l = cur.leaf
                force = force + m(l) / norm(Y(l,:) - X(i,:)); % direct evaluation
            end
        elseif cur.r < theta * norm(X(i,:) - [cur.xc, cur.yc, cur.zc]) 
            force = force + evalPotential(i, cur.multipole, cur.xc, cur.yc, cur.zc);
        else
            for j = 1:cur.nchild
                force = helper(cur.child(j), i, force);
            end
        end
    end



end