function multipole = p2m( ind , xc, yc, zc)
% see notebook Ritz 108

global Y m
multipole = zeros(10,1);
multipole(1) = sum(m(ind));
multipole(2) = sum(m(ind).* (xc-Y(ind,1)));
multipole(3) = sum(m(ind).* (yc-Y(ind,2)));
multipole(4) = sum(m(ind).* (zc-Y(ind,3)));
multipole(5) = sum(m(ind).* (xc-Y(ind,1)).^2/2);
multipole(6) = sum(m(ind).* (yc-Y(ind,2)).^2/2);
multipole(7) = sum(m(ind).* (zc-Y(ind,3)).^2/2);
multipole(8) = sum(m(ind).* (xc-Y(ind,1)).*(yc-Y(ind,2))/2);
multipole(9) = sum(m(ind).* (yc-Y(ind,2)).*(zc-Y(ind,3))/2);
multipole(10) = sum(m(ind).* (xc-Y(ind,1)).*(zc-Y(ind,3))/2);
end
