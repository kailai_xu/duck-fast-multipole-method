classdef fmmCell < handle
    % FMMCELL a class that holds the basic structure of quad tree. See
    % reference
    properties
        xc
        yc
        zc
        r
        nleaf
        leaf
        nchild
        child
        parent
        multipole
        id
    end
    
    
    methods
        function obj = fmmCell(bmin, bmax, I, minP, maxLevel)
            tic
            %             assert(bmax(1)-bmin(1) == bmax(2)-bmin(2))
            %             assert(bmax(1)-bmin(1) == bmax(3)-bmin(3))
            obj.r = bmax(1)-bmin(1);
            obj.xc = mean([bmax(1) bmin(1)]);
            obj.yc = mean([bmax(2) bmin(2)]);
            obj.zc = mean([bmax(3) bmin(3)]);
            
            constructTree(obj, I, minP,maxLevel-1);
            
            function constructTree(p, I, minP,maxLevel)
                persistent curid
                if isempty(curid)
                    curid = 0;
                end
                curid = curid + 1;
                p.id = curid;
                
                global Y
                if length(I) <= minP || maxLevel==0
                    p.nleaf = length(I);
                    p.leaf = I;
                    p.nchild = 0;
                    p.child = {};
                else
                    x = [p.xc -  p.r/2;
                        p.xc;
                        p.xc +  p.r/2];
                    
                    y = [p.yc -  p.r/2;
                        p.yc;
                        p.yc +  p.r/2];
                    
                    z = [p.zc -  p.r/2;
                        p.zc;
                        p.zc +  p.r/2];
                    
                    p.child = {};
                    for i = 1:2
                        for j = 1:2
                            for k = 1:2
                                I0 = (Y(I,1)>x(i)) & (Y(I,2) > y(j)) & (Y(I,3) > z(k)) ...
                                    & (Y(I,1)<=x(i+1)) & (Y(I,2) <= y(j+1)) & (Y(I,3) <= z(k+1));
                                I0 = I(I0);
                                if isempty(I0)
                                    continue
                                end
                                c = fmmCell([x(i) y(j) z(k)], [x(i+1) y(j+1) z(k+1)], I0, minP, maxLevel);
                                p.child = [p.child c];
                            end
                        end
                    end
                    p.nchild = length(p.child);
                    p.nleaf = 0;
                    p.leaf = [];
                    
                end
            end
            
            
            
        end
        
        function plotCell(obj)
            global Y
            figure; hold on
            
            helper(obj);
            xlabel('x'); ylabel('y'); zlabel('z');
            xlim([0,1])
            ylim([0,1])
            zlim([0,1])
            function helper(p)
                if p.nleaf
                    scatter3(Y(p.leaf,1), Y(p.leaf,2), Y(p.leaf,3), '*');
                else
                    for i = 1:p.nchild
                        helper(p.child(i));
                    end
                end
            end
        end
        
        
        
        
        
        function C = constructCellList(obj)
            curid = 0;
            nleaf0 = 0;
            helper1(obj);
            function helper1(p)
                curid = curid+1;
                if p.nchild
                    for j = 1:p.nchild
                        helper1(p.child(j));
                    end
                end
            end
            
            C = cell(curid,1);
            i = 1;
            helper2(obj);
            function helper2(p)
                C{i} = p;
                i = i+1;
                
                if p.nchild
                    for j = 1:p.nchild
                        helper2(p.child(j));
                        p.child(j).parent = i;
                    end
                else
                    nleaf0 = nleaf0 + 1;
                end
            end
            fprintf('Number of cells:%d, number of leaf cells: %d\n', curid, nleaf0);
        end
    end
end
