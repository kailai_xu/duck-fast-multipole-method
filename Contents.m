% DUCK-FAST-MULTIPOLE-METHOD
%
% Files
%   evalPotential         - targets: the list of target objects in Particle
%   fmmCell               - a class that holds the basic structure of quad tree. See
%   FMMEvalPotential      - Evaluate the gravitational potential at a target point i,
%   get_multipole         - Calculate multipole arrays for all leaf cells under cell p.
%   m2m                   - see notebook Ritz 108
%   p2m                   - see notebook Ritz 108
%   test_directsum        - 
%   test_fmmCell          - 
%   test_FMMEvalPotential - 
%   test_m2m              - 
%   test_p2m              - 
%   test_upward_sweep     - 
%   upward_sweep          - computes the multipoles of parent nodes from the leaf nodes
